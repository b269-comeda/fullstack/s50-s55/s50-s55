import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
	// const data = {
	// 	title: "Error 404 - Page Not Found.",
	// 	content: "The page you are looking for cannot be found.",
	// 	destination: "/",
	// 	label: "Back to Home"
	// }


	return (
		<>
			{/*< Banner/>*/}
			< Banner notFound = {false}/>
			{/*< Banner data={data}/>*/}
      		< Highlights/>
		</>
	)
}




