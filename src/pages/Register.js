import {useState, useEffect, useContext} from 'react';

// s54 activity start
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
// s54 activity end

import { Form, Button } from 'react-bootstrap';

export default function Register() {

	// s54 activity start
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	// s54 activity end

	// s55 activity start
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	// s55 activity end

	// to store and manage value of the input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (( firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && password1 === password2) {
			setIsActive(true);
		} else {
			setIsActive(false);
		};
	}, [firstName, lastName, mobileNo, email, password1, password2]);

	// function to simulate user registration
	function registerUser(e) {
		e.preventDefault();

		// s54 activity start
		// checking if email exists
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data !== true) {
				// registering the user to mongoDB
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if (data === true) {
						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate("/login")
					} else {
						Swal.fire({
							title: "Duplicate email found",
							icon: "error",
							text: "Please provide a different email."
						})
					}
				})

				navigate("/login")
			} else {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email."
				})
			}
		})

		
		// s55 activity end

		// Clear input fields
		setEmail("");
		setFirstName("");
		setLastName("");
		setMobileNo("");
		setPassword1("");
		setPassword2("");

		// alert("Thank you for registering!");
	};
	
    return (
    	// s54 activity start
    	(user.id !== null)?
    	<Navigate to="/courses"/>
    	:
    	// s54 activity end
        <Form onSubmit={(e) => registerUser(e)}>

        	{/*s55 activity start*/}
        	<Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter first name" 
	                value={firstName}
	                onChange={e => setFirstName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter last name" 
	                value={lastName}
	                onChange={e => setLastName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userMobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter Mobile Number" 
	                value={mobileNo}
	                onChange={e => setMobileNo(e.target.value)}
	                required
                />
            </Form.Group>
        	{/*s55 activity end*/}

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password1}
	                onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
	                value={password2}
	                onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>

            {isActive ?
            <Button variant="primary" type="submit" id="submitBtn">
            	Submit
            </Button>
            	:
        	<Button variant="danger" type="submit" id="submitBtn" disabled>
        		Submit
        	</Button>
        	}
            
        </Form>
    )

}






