/*import { Button, Col, Row, Card } from 'react-bootstrap';

export default function CourseCard() {
	return (
		<Row className="mt-3 mb-3">
		    <Col xs={12} md={4} lg={12}>
		        <Card className="cardHighlight p-0">
		            <Card.Body>
		                <Card.Title>
		                    <h4>Sample Course</h4>
		                </Card.Title>
		                <Card.Text>
		                  <p><b>Description:</b></p>
		                  <p>This is a sample course offering.</p>
		                  <p><b>Price:</b></p>
		                  <p>PHP 40,000</p>
		                </Card.Text>
		                <Button variant="primary">Enroll</Button>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}
*/

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]

import {useState, useEffect} from 'react';

import {Link} from 'react-router-dom';

// [S50 ACTIVITY]
export default function CourseCard({course}) {

	// Deconstruct the course properties into their own variables
	const { name, description, price, _id } = course;

	/*
	SYNTAX: 
		const [getter, setter] = useState(initialGetterValue);
	*/
// 	const [count, setCount] = useState(0);
// 	// s51 activity start
// 	// const [seats, setSeats] = useState(30);
// 	// s51 activity end
// 	const [seats, setSeats] = useState(5);

// 	function enroll() {
// 		// setCount(count + 1);
// 		// s51 activity start
// 		/*setSeats(seats - 1);
// 		if (seats <= 1) {
// 			alert("No more seats");
// 			document.getElementById("enroll-btn").disabled = true;
// 		}*/
// 		// s51 activity end
// 		// activity s51 solution start
// 		/*if (seats > 0) {
// 			setCount(count + 1);
// 			setSeats(seats - 1);
// 		} else {
// 			alert("No more seats");
// 			document.getElementById("enroll-btn").disabled = true;
// 		}*/
// 		// activity s51 solution end

// 		setCount(count + 1);
// 		setSeats(seats - 1);
// 	};

// useEffect(() => {
// 	if (seats <= 0) {
// 		alert("No more seats available!");
// 	}
// }, [seats]);
	
return (
    <Row className="mt-3 mb-3">
        <Col xs={12}>
            <Card className="cardHighlight p-0">
                <Card.Body>
                    <Card.Title><h4>{name}</h4></Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    {/*<Card.Subtitle>Count: {count}</Card.Subtitle>*/}
                    {/*ACTIVITY S51 START*/}
                   {/* <Card.Subtitle>Enrollees:</Card.Subtitle>
                    <Card.Text>{count} Enrollees</Card.Text>*/}
                    {/*<Button id="enroll-btn" variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}
                    {/*ACTIVITY S51 END*/}
                   	<Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                </Card.Body>
            </Card>
        </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]
