// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';


import { Button, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
// import { Link } from 'react-router-dom';


export default function Banner({notFound}) {

    const navigate = useNavigate();

    function backHome(e) {
        navigate('/');
    }

    return (
        notFound ?
        <Row>
            <Col className="p-5">
                <h1>Error 404 - Page not found.</h1>
                <p>The page you are looking for cannot be found.</p>
                <Button onClick={e => backHome(e)} variant="primary">Back to Home</Button>
            </Col>
        </Row>
        :
        <Row>
        	<Col className="p-5">
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button variant="primary">Enroll now!</Button>
            </Col>
        </Row>
    )
}


// export default function Banner({data}) {

//     const { title, content, destination, label } = data;

//     return (
//         <Row>
//             <Col className="p-5">
//                 <h1>{title}</h1>
//                 <p>{content}</p>
//                 <Button as={Link} to={destination} variant="primary">{label}</Button>
//             </Col>
//         </Row>
//     )

// }